/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ust.spi;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author nuwan
 */
public class EntityTest {
  
  public EntityTest() {
  }
  
  @BeforeClass
  public static void setUpClass() {
  }
  
  @AfterClass
  public static void tearDownClass() {
  }
  
  @Before
  public void setUp() {
  }
  
  @After
  public void tearDown() {
  }

  /**
   * Test of getID method, of class Entity.
   */
  @Test
  public void testGetID() {
  
    TestEntity entity = new TestEntity("test", 12, "sfs");
    entity.apply("nuwan");
    assertEquals("nuwan", entity.name);
  }

 

  public class EntityImpl extends Entity {

    public EntityImpl() {
      super("");
    }
  }
  
}

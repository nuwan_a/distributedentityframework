/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ust.spi;

/**
 *
 * @author nuwan
 */
public class TestEntity extends Entity{
  String name;
  int age;

  public TestEntity(String name, int age, String id) {
    super(id);
    this.name = name;
    this.age = age;
  }
  
  public void apply(String name)
  {
    this.name = name;
  }
  
}

package com.ust.spi;

public interface EntityViewRespository<TEntity> {

  TEntity getEntity(String... keys) throws Exception;
}

package com.ust.spi;

import java.util.Arrays;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import java.util.stream.Collectors;

public class EntityRepository<TEntity extends Entity> implements EntityViewRespository<TEntity>{
    Map<String,TEntity> mapEntityCache = new ConcurrentHashMap<>();
    
    @Override
    public TEntity getEntity(String... keys) throws Exception
    {
      String key = Arrays.stream(keys).collect(Collectors.joining(":"));
      return mapEntityCache.get(key);
    }
    
    public  void saveEntity(TEntity entity) throws Exception
    {
      mapEntityCache.put(entity.getID(), entity);
    }
}

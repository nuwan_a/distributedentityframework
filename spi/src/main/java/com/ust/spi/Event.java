package com.ust.spi;

public abstract class Event {

  String entityID;

  public Event() {
  }

  protected void setEntityID(String entityID) {
    this.entityID = entityID;
  }
  
  public String getEntityID() {
    return this.entityID;
  }
}

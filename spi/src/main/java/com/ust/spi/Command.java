package com.ust.spi;

public interface Command<TResult> {
  String getKey();
}

package com.ust.spi;

import com.ust.spi.annotation.Inject;

/**
 *
 * @param <TCommand>
 * @param <TResult>
 * @param <TEntity>
 */
public abstract class EntityCommandHandler<TCommand extends Command<TResult>, TResult, TEntity extends Entity>
    implements CommandHandler<TCommand, TResult> {

  @Inject
  EntityRepository<TEntity> entityRepo = new EntityRepository<>();

  @Override
  public abstract TResult execute(TCommand cmd) throws Exception;

  protected EntityRepository<TEntity> getRepository() {
    return entityRepo;
  }
}

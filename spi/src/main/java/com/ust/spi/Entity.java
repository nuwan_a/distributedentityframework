package com.ust.spi;

import java.lang.reflect.Method;
import java.util.Arrays;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.function.Function;
import java.util.stream.Collectors;


/**
 *
 * @param <EntityRoot>
 */
public abstract class Entity<EntityRoot extends Entity> {

  private final String id;
  private long version;
  private List<Event> events;
  private static final Map<Class<?>, Method> map;

  static {
    map = buildApplyMap();
  }

  public Entity(String id) {
    this.id = id;
    this.version = 0;
    events = new LinkedList<>();

  }

  public final String getID() {
    return id;
  }

  public void applyEvent(Event event) {
    event.setEntityID(id);
    events.add(event);
  }

  private static Map<Class<?>, Method> buildApplyMap() {
    Method[] methods = new Object() { }.getClass().getEnclosingClass().getMethods();
    return Arrays.asList(methods).stream().filter(method -> method.getName().equals("apply"))
        .collect(Collectors.toMap(method -> {
          return method.getParameterTypes()[0];
        }, Function.identity()));
  }

}

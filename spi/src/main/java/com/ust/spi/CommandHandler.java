package com.ust.spi;

/**
 * @param <TCommand>
 * @param <TResult>
 */
public interface CommandHandler<TCommand extends Command<TResult>, TResult> {

  TResult execute(TCommand cmd) throws Exception;
}
